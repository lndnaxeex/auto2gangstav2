﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(PrefabSelector))]
public class PrefabSelectorEditor : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
             
        var resourcesPath = Path.Combine(Application.dataPath, "Resources");
        var absolutePaths = Directory.GetFiles(resourcesPath, "*.prefab", SearchOption.AllDirectories);
        var paths = new string[absolutePaths.Length];
        for (var x = 0 ; x < absolutePaths.Length; x++)
        {
            var absolutePath = absolutePaths[x];
                
            var path = absolutePath.Replace(resourcesPath + Path.DirectorySeparatorChar, "");
            path = path.Substring(0, path.Length - 7).Replace("\\", "/");
            paths[x] = path;
        }

        var stringProperty = property.FindPropertyRelative("PrefabPath");
           
        stringProperty.stringValue = paths[EditorGUI.Popup(position, label.text, Mathf.Max(Array.IndexOf(paths, stringProperty.stringValue), 0), paths)];
        EditorGUI.EndProperty();
    }
}
