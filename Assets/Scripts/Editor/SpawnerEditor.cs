using InteractiveObjects.Spawn;
using UnityEditor;
using UnityEngine;

public static class HandleUtils
{
    public static void DrawSolidCubeWithOutline(Matrix4x4 transform, Vector3 scale, Color color1, Color color2)
    {
        transform *= Matrix4x4.Translate(new Vector3(0, scale.y, 0));
        var v = new[]
        {
            transform.MultiplyPoint(new Vector3(scale.x, -scale.y, scale.z)), // 0
            transform.MultiplyPoint(new Vector3(-scale.x, -scale.y, scale.z)), // 1
            transform.MultiplyPoint(new Vector3(scale.x, scale.y, scale.z)), // 2
            transform.MultiplyPoint(new Vector3(-scale.x, scale.y, scale.z)), // 3
            transform.MultiplyPoint(new Vector3(-scale.x, -scale.y, -scale.z)), //4 
            transform.MultiplyPoint(new Vector3(scale.x, -scale.y, -scale.z)), //5 
            transform.MultiplyPoint(new Vector3(-scale.x, scale.y, -scale.z)), //6
            transform.MultiplyPoint(new Vector3(scale.x, scale.y, -scale.z)) //7
        };
        
        Handles.DrawSolidRectangleWithOutline(new []{v[0], v[1], v[4], v[5]}, color1, color2);
        Handles.DrawSolidRectangleWithOutline(new []{v[2], v[3], v[6], v[7]}, color1, color2);
        Handles.DrawSolidRectangleWithOutline(new []{v[0], v[2], v[7], v[5]}, color1, color2);
        Handles.DrawSolidRectangleWithOutline(new []{v[1], v[3], v[6], v[4]}, color1, color2);
        Handles.DrawSolidRectangleWithOutline(new []{v[0], v[2], v[3], v[1]}, color1, color2);
        Handles.DrawSolidRectangleWithOutline(new []{v[4], v[5], v[7], v[6]}, color1, color2);
    }
}

[CustomEditor(typeof(HumanPlayerSpawner))]
public class SpawnerEditor : Editor
{
    [DrawGizmo(GizmoType.InSelectionHierarchy | GizmoType.NotInSelectionHierarchy)]
    static void DrawHandles(HumanPlayerSpawner targetObject, GizmoType gizmoType)
    {
        var color1 = new Color(1f, 0.5f, 0.5f, 0.2f);
        var color2 = new Color(0, 0, 0, 1);
        HandleUtils.DrawSolidCubeWithOutline(targetObject.transform.localToWorldMatrix, new Vector3(1, 1.4f, 1), color1, color2);
    }
}