﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(SceneSelector))]
 public class SceneSelectorEditor : PropertyDrawer
 {
     public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
     {
         EditorGUI.BeginProperty(position, label, property);
         
         var filePaths = Array.ConvertAll(AssetDatabase.FindAssets("t:Scene"), AssetDatabase.GUIDToAssetPath);
         var paths = new string[filePaths.Length + 1];
         paths[0] = "<undefined>";
         for (var x = 1; x < paths.Length; x++) paths[x] = Path.GetFileNameWithoutExtension(filePaths[x - 1]);
         var stringProperty = property.FindPropertyRelative("SceneName");
         // = EditorGUILayout.Popup(label, , paths)];
     
         stringProperty.stringValue = paths[EditorGUI.Popup(position, label.text, Mathf.Max(Array.IndexOf(paths, stringProperty.stringValue), 0), paths)];
         EditorGUI.EndProperty();
     }
 }