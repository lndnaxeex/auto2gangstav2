﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicHealthBar : MonoBehaviour
{
    [SerializeField] HumanPlayerStatus _humanPlayerStatus;
    RectTransform _rectTransform;
    
    void Start()
    {
        _rectTransform = GetComponent<RectTransform>();
    }

    void Update()
    {
        _rectTransform.localScale = new Vector3(_humanPlayerStatus.Health * 0.01f, 1f, 1f);
    }
}
