using System;
using UnityEngine;


namespace Cameras
{
    public abstract class CameraLogic : ScriptableObject
    {
        protected CameraStatus _cameraStatus; 
        protected Transform m_Cam { get { return _cameraStatus.Camera.transform; } }
        public virtual void Setup(CameraStatus cameraStatus)
        {
            _cameraStatus = cameraStatus;
            if (!m_Cam.parent)
            {
                SetupAsCameraRig();
            }   
        }

        void SetupAsCameraRig()
        {
            var rigTransform = new GameObject("CameraRig");
            var pivotTransform = new GameObject("Pivot");
            m_Cam.position = Vector3.zero;
            m_Cam.SetParent(pivotTransform.transform);
            pivotTransform.transform.SetParent(rigTransform.transform);
        }
        public abstract void Update();
    }


    public abstract class PivotBasedCameraRig : CameraLogic
    {
        // This script is designed to be placed on the root object of a camera rig,
        // comprising 3 gameobjects, each parented to the next:

        // 	Camera Rig
        // 		Pivot
        // 			Camera
        protected Transform transform { get {return _rigTransform;}}
        protected Transform m_Pivot; // the point at which the camera pivots around
        protected Transform _rigTransform; // the point at which the camera pivots around
        protected Vector3 m_LastTargetPosition;

        public override void Setup(CameraStatus cameraStatus)
        {
            base.Setup(cameraStatus);
            // find the camera in the object hierarchy
            m_Pivot = m_Cam.parent;
            _rigTransform = m_Pivot.parent;

            // TODO Set those as parameters

            _rigTransform.localPosition = new Vector3(-30, 0, 18);
            m_Pivot.localPosition = Vector3.up * 2f;
            m_Pivot.localRotation = Quaternion.Euler(Vector3.up * 180f);
            m_Cam.localPosition = new Vector3(0f, -.3f, -3.33f);

        }
    }
}
