using System.Collections;
using Boo.Lang;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSettingsContainer : MonoBehaviour
{
    bool _hudLoaded;
    AsyncOperation _asyncLoad;
    [SerializeField] SceneSettings _sceneSettings;

    public void SetupAsAdditiveScene(string thisSceneName)
    {
        // Here the HUD should be merged into the main scene
        if (!_hudLoaded)
        {
            StartCoroutine(AddHud(_sceneSettings.HudSceneName, thisSceneName));
        }

    }

    IEnumerator AddHud(string name, string destinationScene)
    {
        _asyncLoad = SceneManager.LoadSceneAsync(name, LoadSceneMode.Additive);
        while (!_asyncLoad.isDone) 
            yield return null;
        
        var currentScene = SceneManager.GetActiveScene();
        var nonContainerTransforms = new List<Transform>();
        var destinationSceneObject = SceneManager.GetSceneByName(destinationScene);
        var rootGameObjects = destinationSceneObject.GetRootGameObjects();

        SceneManager.MergeScenes(SceneManager.GetSceneByName(name), destinationSceneObject);
        SceneManager.SetActiveScene(destinationSceneObject);
        
        Transform containerTransform = null;
        
        for (var x = 0; x < rootGameObjects.Length; x++)
        {
            var rootGoName = rootGameObjects[x].name;
            if (rootGoName == GameBehaviour.GetSceneContainerName(destinationSceneObject.name)) 
                containerTransform = rootGameObjects[x].transform;
            else nonContainerTransforms.Add(rootGameObjects[x].transform);
        }
        
        // Only perform re-parenting when the scene is loaded for "Game" Monobehaviour
        if (containerTransform != null)
        {
            var hudContainer = new GameObject("_HUD");
            for (var x = 0; x < nonContainerTransforms.Count; x++)
                nonContainerTransforms[x].SetParent(hudContainer.transform);
            
            hudContainer.transform.SetParent(containerTransform);
        }

        SceneManager.SetActiveScene(currentScene);
        _hudLoaded = true;
        _asyncLoad = null;
    }

    void Update()
    {
        if (!_hudLoaded && !_sceneSettings.AsLoadableScene)
        {
            StartCoroutine(AddHud(_sceneSettings.HudSceneName, SceneManager.GetActiveScene().name));

        }

    }

}
