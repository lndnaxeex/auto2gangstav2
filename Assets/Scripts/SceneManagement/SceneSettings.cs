using UnityEngine;

[CreateAssetMenu]
public class SceneSettings : ScriptableObject
{
    [SerializeField] bool _asLoadableScene = true;
    [SerializeField] SceneSelector _hud;

    public bool AsLoadableScene
    {
        get { return _asLoadableScene; }
    }

    public string HudSceneName
    {
        get { return _hud.SceneName; }
    }
}