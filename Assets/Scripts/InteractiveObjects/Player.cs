using UnityEngine;
using Cameras;

public class Player : MonoBehaviour
{
    [SerializeField] HumanPlayerStatus _humanPlayerStatus;
    [SerializeField] CameraStatus _cameraStatus;
    
    public void Setup(HumanPlayerStatus humanPlayerStatus, CameraStatus cameraStatus)
    {
        _humanPlayerStatus = humanPlayerStatus;
        _cameraStatus = cameraStatus;
        
        var camera = FindObjectOfType<Camera>();
        _cameraStatus.Setup(camera, transform);
        _humanPlayerStatus.Setup(transform);
    }

    void FixedUpdate()
    {
        _humanPlayerStatus.Update();
        _cameraStatus.UpdateTransform();
    }
}
