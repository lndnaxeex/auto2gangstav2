using UnityEngine;

namespace InteractiveObjects.Spawn
{
    public abstract class SpawnerBase : MonoBehaviour
    {
        [SerializeField] protected InteractiveObject _gameModel;
        protected GameObject _instantiatedGameObject;

        void Start()
        {
            _instantiatedGameObject = Instantiate (Resources.Load (_gameModel.ModelPrefabPath) as GameObject);
            _instantiatedGameObject.transform.position = transform.position;
            SetupGameObjectComponents();
        }

        protected abstract void SetupGameObjectComponents();

    }
}