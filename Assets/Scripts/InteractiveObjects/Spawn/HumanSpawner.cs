using UnityEditor.Animations;
using UnityEngine;

namespace InteractiveObjects.Spawn
{
    public class HumanSpawner : SpawnerBase
    {
        [SerializeField] protected HumanStatus _humanStatus;

        protected override void SetupGameObjectComponents()
        {
            var humanModel = (_gameModel as HumanObject);

            _humanStatus.Stats = humanModel;
            // _humanStatus.AnimatorController = new AnimatorController() {name = "Runtime animator controller"};
            // _humanStatus.AnimatorController.AddLayer("Main");

            var animatorComponent = _instantiatedGameObject.GetComponent<Animator>();
            if (!animatorComponent) animatorComponent = _instantiatedGameObject.AddComponent<Animator>();
            var ridigBodyComponent = _instantiatedGameObject.GetComponent<Rigidbody>();
            if (!ridigBodyComponent) ridigBodyComponent = _instantiatedGameObject.AddComponent<Rigidbody>();
            var capsuleCollider = _instantiatedGameObject.GetComponent<CapsuleCollider>();
            if (!capsuleCollider) capsuleCollider = _instantiatedGameObject.AddComponent<CapsuleCollider>();

            _humanStatus.Components.Animator = animatorComponent;
            _humanStatus.Components.CapsuleCollider = capsuleCollider;
            _humanStatus.Components.Rigidbody = ridigBodyComponent;

            SetupInteractiveObject();
        }

        protected virtual void SetupInteractiveObject()
        {
            
        }
    }
}
