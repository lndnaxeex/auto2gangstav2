using UnityEngine;
using Cameras;

namespace InteractiveObjects.Spawn
{
    public class HumanPlayerSpawner : HumanSpawner
    {
        [SerializeField] CameraStatus _cameraStatus;

        protected override void SetupInteractiveObject()
        {
            var playerComponent = _instantiatedGameObject.AddComponent<Player>();
            playerComponent.Setup(_humanStatus as HumanPlayerStatus, _cameraStatus);
        }
    }
}