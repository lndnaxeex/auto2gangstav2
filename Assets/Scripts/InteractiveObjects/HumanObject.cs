using Behaviors;
using Behaviors.Human;
using UnityEngine;

public abstract class InteractiveObject : ScriptableObject
{
    [SerializeField] PrefabSelector _model;
    public string ModelPrefabPath
    {
        get { return _model.PrefabPath; }
    }
}

public struct HumanBehaviourComponents 
{
    public Transform Transform;
    public Rigidbody Rigidbody;
    public Animator Animator;
    public CapsuleCollider CapsuleCollider;
}

[CreateAssetMenu]
public class HumanObject : InteractiveObject
{
    [SerializeField] float _health;
    [SerializeField] float _movementSpeed;
    [SerializeField] HumanBehaviorProfile _behaviorProfile;
    
    public HumanBehaviorProfile BehaviorProfile
    {
        get { return _behaviorProfile; }
    }

    public float MovementSpeed => _movementSpeed;
}
