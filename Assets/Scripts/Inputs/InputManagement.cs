﻿using System.Collections;
using System.Collections.Generic;
using Inputs.Touch;
using UnityEngine;

// A convenient wrapper for reading inputs
// to simulate a basic gamepad
namespace Inputs
{
    
    public class Management
    {
        static Management _current;
        bool _touchInputsActive;

        TouchLayoutHandler _touchLayoutHandler;
        public TouchLayoutHandler TouchLayoutHandler 
        { 
            set 
            {
                _touchLayoutHandler = value;
                _touchInputsActive = true;
            }
        }

        public static Management Current
        {
            get { return _current ?? (_current = new Management()); }
        }
        
        public float Get(Type inputType)
        {
            switch (inputType)
            {
                case Type.DPAD_UP:
                case Type.DPAD_DOWN:
                case Type.DPAD_RIGHT:
                case Type.DPAD_LEFT:
                case Type.START:
                case Type.SELECT:
                case Type.MENU:
                case Type.OPTION1:
                case Type.OPTION2:
                case Type.OPTION3:
                case Type.OPTION4:
                case Type.L1: case Type.L2: case Type.L3:
                case Type.R1: case Type.R2: case Type.R3:
                    return HandleButton(inputType);
                case Type.LEFT_H: case Type.LEFT_V: 
                    return HandleJoystick(inputType, Type.JOYSTICK_LEFT);
                case Type.RIGHT_H: case Type.RIGHT_V:
                    return HandleJoystick(inputType, Type.JOYSTICK_LEFT);
            }

            return 0f;
        }

        float HandleJoystick(Type inputType, Type joystickType)
        {
            var joystickValue = _touchInputsActive ? _touchLayoutHandler.Current.GetValue(inputType) : 0f;
            
            if (inputType == Type.LEFT_H)
            {
                joystickValue += Input.GetAxis("Horizontal");
            }
            
            if (inputType == Type.LEFT_V)
            {
                joystickValue += Input.GetAxis("Vertical");
            }
            if (_touchInputsActive) return joystickValue;

            if (inputType == Type.RIGHT_H)
            {
                joystickValue += Input.GetAxis("Mouse X");
            }
            
            if (inputType == Type.RIGHT_V)
            {
                joystickValue += Input.GetAxis("Mouse Y");
            }

            return joystickValue;
        }

        float HandleButton(Type inputType)
        {
            return 0f;//throw new System.NotImplementedException();
        }
    }
}
