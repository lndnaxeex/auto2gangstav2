using UnityEngine;

namespace Inputs.Touch
{
    /// <summary>
    /// Used as a type definition for each touch layout.
    /// </summary>
    [CreateAssetMenu]
    public class TouchLayoutReference : ScriptableObject
    {
        bool _active;

        [SerializeField] SceneSelector _sceneName;
        TouchControlLayout _touchControlLayout;
        TouchLayoutHandler _touchLayoutHandler;

        public void SetLoaded(TouchControlLayout touchControlLayout, TouchLayoutHandler handlerReference)
        {
            _touchControlLayout = touchControlLayout;
            _touchLayoutHandler = handlerReference;
            SetDisplayed(_active);
        }

        public void SetActive(bool active)
        {
            _active = active;
            SetDisplayed(active);
        }

        void SetDisplayed(bool displayed)
        {
            if (_touchControlLayout)
                _touchControlLayout.Enabled = displayed;
            
            if (displayed && _touchLayoutHandler)
                _touchLayoutHandler.Current = this;
        }

        public string SceneName
        {
            get { return _sceneName.SceneName; }
        }

        public float GetValue(Type inputType)
        {
            return _touchControlLayout.GetValue(inputType);
        }
    }
}