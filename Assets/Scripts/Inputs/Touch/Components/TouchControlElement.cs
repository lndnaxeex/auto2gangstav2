using Inputs;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Inputs.Touch.Components
{
    public abstract class TouchControlElement : MonoBehaviour, IPointerDownHandler
    {
        [SerializeField] protected Inputs.Type _mapping;
        protected RectTransform _rectTransform;

        public Type Mapping
        {
            get { return _mapping; }
        }

        void Awake()
        {
            _rectTransform = GetComponent<RectTransform>();
        }

        public abstract void OnPointerDown(PointerEventData eventData);
    }
}