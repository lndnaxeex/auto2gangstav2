using UnityEngine;
using UnityEngine.EventSystems;

namespace Inputs.Touch.Components
{
    public class Joystick : TouchControlElement, IPointerDownHandler
    {
        /// Current value of the controls.
        public Vector2 Delta
        {
            get { return _delta; }
        }

        /// Current magnitude of input.
        public float Magnitude
        {
            get { return _magnitude; }
        }

        /// Is the current input cancelled.
        public bool IsCancelled
        {
            get { return _isCancelled; }
        }

        /// Link to the object that is always shown.
        [Tooltip("Link to the object that is always shown.")]
        public GameObject Control;

        /// Link to the object that is only shown when pressed.
        [Tooltip("Link to the object that is only shown when pressed.")]
        public GameObject Selection;

        /// Link to the object that is only shown when pressed and represents the center.
        [Tooltip("Link to the object that is only shown when pressed and represents the center.")]
        public GameObject Center;

        /// Relative size of the screen area at the same side as the touch control.
        [Tooltip("Relative size of the screen area at the same side as the touch control.")]
        public float ScreenAreaSize = 0.5f;

        /// Distance from the control to it's center circle. Value is relative to the screen height.
        [Tooltip("Distance from the control to it's center circle. Value is relative to the screen height.")]
        public float CenterDistance = 0.1f;

        /// Cancel input when the distance from the control to the center is lesser than the value. Value is relative to the screen height.
        [Tooltip(
            "Cancel input when the distance from the control to the center is lesser than the value. Value is relative to the screen height.")]
        public float CancelDistance = 0.05f;

        Vector2 _delta = Vector2.zero;
        Vector2 _center = Vector2.zero;
        float _magnitude = 0;
        bool _isPressed;
        bool _isActive;
        Vector2 _pixel;
        int _touchId;
        bool _isCancelled;
        Vector2 _previousMousePosition;
        bool _previousMouseDown;

        public override void OnPointerDown(PointerEventData eventData)
        {
            if (_isPressed)
                return;

            if (IsInside(eventData.position))
            {
                _isPressed = true;
                _isActive = false;
                _touchId = eventData.pointerId;

                if (eventData.delta.magnitude > 1)
                    ProcessDelta(eventData.position, eventData.delta);
            }
        }

        protected virtual void Update()
        {
            var wasPressed = _isPressed;
            _isPressed = false;

            if (!wasPressed)
                _center = new Vector2(transform.position.x, transform.position.y);

            var mouseDelta = new Vector2(Input.mousePosition.x, Input.mousePosition.y) - _previousMousePosition;
            _previousMousePosition = Input.mousePosition;

            var wasMouseDown = _previousMouseDown;
            _previousMouseDown = Input.GetMouseButton(0);

            var newTouchId = -10;

            if (Application.isMobilePlatform)
            {
                for (var i = 0; i < Input.touchCount; i++)
                {
                    var touch = Input.GetTouch(i);

                    if (Process(touch.fingerId, touch.position, touch.deltaPosition, wasPressed,
                        touch.phase == TouchPhase.Began))
                    {
                        newTouchId = touch.fingerId;
                        break;
                    }
                }
            }
            else if (Input.GetMouseButton(0))
            {
                var mouse = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

                if (Process(-1, mouse, mouseDelta, wasPressed, !wasMouseDown))
                    newTouchId = -1;
            }

            _touchId = newTouchId;

            if (!_isPressed)
            {
                _delta = Vector2.zero;
                _isActive = false;
            }

            if (_isPressed && _isActive)
            {
                if (Control != null)
                    Control.transform.position = new Vector3(_pixel.x, _pixel.y, Control.transform.position.z);

                if (Selection != null)
                {
                    Selection.transform.position = new Vector3(_pixel.x, _pixel.y, Selection.transform.position.z);

                    if (!Selection.activeSelf)
                        Selection.SetActive(true);
                }

                if (Center != null)
                {
                    Center.transform.position = new Vector3(_center.x, _center.y, Center.transform.position.z);

                    if (!Center.activeSelf)
                        Center.SetActive(true);
                }
            }
            else
            {
                if (Control != null)
                    Control.transform.position = transform.position;

                if (Selection != null && Selection.activeSelf)
                    Selection.SetActive(false);

                if (Center != null && Center.activeSelf)
                    Center.SetActive(false);
            }
        }

        bool Process(int id, Vector2 pixel, Vector2 delta, bool wasPressed, bool canBegin)
        {
            var isMe = wasPressed && _touchId == id;
            var isBusy = id < 0
                ? EventSystem.current.IsPointerOverGameObject()
                : EventSystem.current.IsPointerOverGameObject(id);
            var canBeginPressing = canBegin && !wasPressed && !isBusy && IsInside(pixel);

            if (isMe || canBeginPressing)
            {
                if (!_isActive)
                {
                    if (delta.magnitude > 1)
                        ProcessDelta(pixel, delta);
                    else
                        _isPressed = true;
                }
                else
                    ProcessPixel(pixel);

                return true;
            }
            else
                return false;
        }

        bool IsInside(Vector2 pixel)
        {
            if (transform.position.x < Screen.width * 0.5f)
            {
                if (pixel.x > Screen.width * ScreenAreaSize)
                    return false;
            }
            else
            {
                if (pixel.x < Screen.width - Screen.width * ScreenAreaSize)
                    return false;
            }

            return true;
        }

        void ProcessDelta(Vector2 pixel, Vector2 delta)
        {
            _pixel = pixel;
            _delta = delta.normalized;
            _isPressed = true;
            _isActive = true;
            _isCancelled = false;
            _center = _pixel - _delta * CenterDistance * Screen.height;
        }

        void ProcessPixel(Vector2 pixel)
        {
            _pixel = pixel;

            _delta = pixel - _center;
            _isPressed = true;
            _isActive = true;

            var dist = _delta.magnitude;

            if (dist > float.Epsilon)
                _delta.Normalize();

            var centerOffset = CenterDistance * Screen.height;
            var cancelOffset = CancelDistance * Screen.height;

            _isCancelled = dist < cancelOffset;
            _magnitude = 0;

            if (dist > centerOffset)
            {
                _center = _pixel - _delta * centerOffset;

                if (!_isCancelled)
                    _magnitude = 1;
            }
            else if (!_isCancelled)
                _magnitude = (dist - cancelOffset) / (centerOffset - cancelOffset);
        }
    }
}