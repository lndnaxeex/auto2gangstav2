using UnityEngine;

namespace Inputs.Touch
{
    /// <summary>
    /// Used to track which layout is current. 
    /// </summary>
    [CreateAssetMenu]
    public class TouchLayoutHandler : ScriptableObject
    {
        public TouchLayoutReference Current { get; set; }
        [SerializeField] TouchLayoutReference[] _touchLayouts;
        
        public string[] InputLayoutSceneNames
        {
            get
            {
                var inputSceneNames = new string[_touchLayouts.Length];
                for (var x = 0; x < _touchLayouts.Length; x++)
                    inputSceneNames[x] = _touchLayouts[x].SceneName;
                return inputSceneNames;
            }
        }

        public void SetLoaded(int index, TouchControlLayout touchControlLayout)
        {
            _touchLayouts[index].SetLoaded(touchControlLayout, this);
        }
    }
}
