using System.Collections.Generic;
using Inputs;
using Inputs.Touch.Components;
using UnityEngine;

/// <summary>
/// This class works as a container of MonoBehaviour UI objects. It's used to retrieve values from the each touch
/// control.
/// </summary>

public class TouchControlLayout : ScriptableObject
{
    public bool Enabled
    {
        get { return _enabled; }
        set
        {
            _enabled = value;
            _container.gameObject.SetActive(value);
        }
    }
    
    [SerializeField] bool _enabled;
    [SerializeField] Dictionary<Type, TouchControlElement> _controls = new Dictionary<Inputs.Type, TouchControlElement>();
    Transform _container;
    
    public void Set(Transform container)
    {
        _container = container;
        var elements = CollectTouchControlElements(_container, new List<TouchControlElement>());
        foreach (var element in elements)
        {
            var touchControlElement = element;
            _controls[touchControlElement.Mapping] = touchControlElement;
            
            switch (touchControlElement.Mapping)
            {
                case Type.JOYSTICK_LEFT:
                    _controls[Type.LEFT_H] = touchControlElement;
                    _controls[Type.LEFT_V] = touchControlElement;
                    break;
                case Type.JOYSTICK_RIGHT:
                    _controls[Type.RIGHT_H] = touchControlElement;
                    _controls[Type.RIGHT_V] = touchControlElement;
                    break;
            }
        }
    }

    static List<TouchControlElement> CollectTouchControlElements(Transform current, List<TouchControlElement> touchControlElements)
    {
        var touchControlElement = current.gameObject.GetComponent<TouchControlElement>();
        if (touchControlElement != null) touchControlElements.Add(touchControlElement);
        foreach (var childTransform in current)
        {
            touchControlElements.AddRange(CollectTouchControlElements(childTransform as Transform, touchControlElements));
        }
        return touchControlElements;
    }
    
    public float GetValue(Type inputType)
    {
        if (!_controls.ContainsKey(inputType)) 
            return 0;
        var touchControlElement = _controls[inputType];
        var joystickElement = (Joystick)touchControlElement ;
        var delta = joystickElement != null ? joystickElement.Delta : Vector2.zero;
        switch (inputType)
        {
            case Type.LEFT_H: case Type.RIGHT_H:
                return delta.x;
            
            case Type.LEFT_V: case Type.RIGHT_V:
                return delta.y;
        }
        return 0;
    }
}
