namespace Inputs
{
    public enum Type
    {
        GENERIC,
        DPAD_UP,
        DPAD_DOWN,
        DPAD_LEFT,
        DPAD_RIGHT,
        START,
        SELECT,
        MENU,
        OPTION1, OPTION2, OPTION3, OPTION4,
        L1, L2, L3,
        R1, R2, R3,
        JOYSTICK_LEFT, 
        JOYSTICK_RIGHT,
        LEFT_H, LEFT_V,
        RIGHT_H, RIGHT_V
    }
}