using UnityEngine;

namespace Cameras
{
    // TODO Split first person third person and vehicle into multiple classes
    [CreateAssetMenu]
    public class CameraStatus : ScriptableObject
    {
        public enum CameraType { FIRST_PERSON, THIRD_PERSON }

        [SerializeField] CameraLogic _cameraLogic;
        // [SerializeField] float _distanceBack = 5f;
        // [SerializeField] float _distanceTop = 3f;
        Camera _camera;
        public Camera Camera { get { return _camera; } }
        
        public bool IsInitialized
        {
            get { return _camera != null; }
        }
        
        Transform _followTarget;
        public Transform FollowTarget { get {return _followTarget; } }

        public void Setup(Camera camera, Transform playerTransform)
        {
            _camera = camera;
            _followTarget = playerTransform;
            _cameraLogic.Setup(this);
        }

        public void UpdateTransform()
        {
            _cameraLogic.Update();
            // _camera.transform.position = _followTarget.position - _followTarget.forward * _distanceBack +
            //                             _followTarget.up * _distanceTop;
            // _camera.transform.LookAt(_followTarget);
        }
    }
}
