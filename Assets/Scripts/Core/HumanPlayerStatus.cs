using System;
using Behaviors.Human;
using Inputs.Touch;
using UnityEditor.Animations;
using UnityEngine;

public abstract class HumanStatus : ScriptableObject, ISerializationCallbackReceiver
{
    [SerializeField] protected float _health;

    protected bool _playerControlled;
    public bool PlayerControlled { get { return _playerControlled; } }
    
    public HumanBehaviourComponents Components;

    protected HumanObject _stats;

    public HumanObject Stats
    {
        get { return _stats; }
        set { _stats = value; }
    }

    protected HumanBehaviorProfile BehaviorProfile => _stats.BehaviorProfile;

    public Transform Transform => Components.Transform;

    [NonSerialized]
    float _currentHealth;
    
    public float Health => _currentHealth;

    public void OnAfterDeserialize()
    {
        _currentHealth = _health;
    }
    
    public void OnBeforeSerialize()
    {
    }

    public void ResetHealth()
    {
        _currentHealth = _health;
    }

    public virtual void Setup(Transform transform)
    {
        Components.Transform = transform;
        BehaviorProfile.Setup(this);
    }

    public abstract void Update();
}

[CreateAssetMenu]
public class HumanPlayerStatus : HumanStatus
{
    [SerializeField] TouchLayoutReference _controlableTouchLayout;
    public override void Setup(Transform transform)
    {
        base.Setup(transform);
        // setup as player controlled behavior
        BehaviorProfile.Setup(this);
        _playerControlled = true;
        if (_controlableTouchLayout)
            _controlableTouchLayout.SetActive(true);
    }

    public override void Update()
    {
        if (BehaviorProfile != null)
            BehaviorProfile.Update();
    }
}
