﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Works essentially as a scene loader and tracks which scenes are and aren't loaded
/// </summary>
public class GameBehaviour : MonoBehaviour
{
    [SerializeField, Expandable] GameState _gameState;
    GameSettings GameSettings
    {
        get { return _gameState.Settings; }
    }
    
    Transform _loadedScene;
    AsyncOperation _asyncLoad;
    bool _areTouchLayoutsLoaded;
    bool UseOnScreenInput { 
        get {
            return GameSettings.UseOnScreenInput && _gameState.TouchLayoutHandler != null; 
        } 
    }
    
    /// <summary>
    /// preform scene loading initialization
    /// </summary>
    void Update()
    {
        if (_asyncLoad != null) return;
        // load initial scene
        if (_loadedScene == null)
        {
            if (GameSettings.InitialSceneName != SceneManager.GetActiveScene().name)
            {
                StartCoroutine(LoadScene(GameSettings.InitialSceneName));
                return;
            }
            _loadedScene = transform;
        }
        
        // load control layouts
        if (UseOnScreenInput && !_areTouchLayoutsLoaded)
        {
            StartCoroutine(LoadTouchScreenLayouts(_gameState.TouchLayoutHandler.InputLayoutSceneNames));
        }
    }

    public static string GetSceneContainerName(string sceneName)
    {
        return "Container_" + sceneName;
    }

    IEnumerator LoadTouchScreenLayouts(string[] sceneNames)
    {
        for (var x = 0; x < sceneNames.Length; x++)
        {
            var sceneName = sceneNames[x];
            _asyncLoad = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            while (!_asyncLoad.isDone) yield return null;
            var container = PutSceneIntoCatalog(sceneName, true);
            var touchControlLayout = ScriptableObject.CreateInstance(typeof(TouchControlLayout)) as TouchControlLayout;
            
            if (touchControlLayout == null) 
                continue;
            
            touchControlLayout.Set(container);
            _gameState.TouchLayoutHandler.SetLoaded(x, touchControlLayout);
        }

        _areTouchLayoutsLoaded = true;
        _asyncLoad = null;
        Inputs.Management.Current.TouchLayoutHandler = _gameState.TouchLayoutHandler;
    }

    IEnumerator LoadScene(string sceneName)
    {
        _asyncLoad = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        while (!_asyncLoad.isDone) 
            yield return null; 
        
        var container = PutSceneIntoCatalog(sceneName);
        
        _loadedScene = container;
        _asyncLoad = null;
    }

    static Transform PutSceneIntoCatalog(string sceneName, bool disable=false)
    {
        var currentScene = SceneManager.GetActiveScene();
        var loadedScene = SceneManager.GetSceneByName(sceneName);
        SceneManager.SetActiveScene(loadedScene);
        var container = new GameObject(GetSceneContainerName(loadedScene.name));
        var rootGameObjects = loadedScene.GetRootGameObjects();

        for (var x = 0; x < rootGameObjects.Length; x++)
        {
            // make sure the camera is ignored
            var rootGameObject = rootGameObjects[x];
            var cameraComponent = rootGameObject.GetComponent<Camera>();
            var sceneSettingsComponent = rootGameObject.GetComponent<SceneSettingsContainer>();

            if (cameraComponent != null)
            {
                Destroy(rootGameObjects[x]);
                continue;
            }

            if (sceneSettingsComponent != null)
            {
                sceneSettingsComponent.SetupAsAdditiveScene(sceneName);
            }
            rootGameObjects[x].transform.SetParent(container.transform);
        }
        
        SceneManager.SetActiveScene(currentScene);
        container.SetActive(!disable);
        return container.transform;
    }
}