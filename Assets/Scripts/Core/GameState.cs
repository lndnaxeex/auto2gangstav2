using Inputs.Touch;
using UnityEngine;

[CreateAssetMenu]
public class GameState : ScriptableObject
{
    [SerializeField, Expandable] GameSettings _gameSettings;
    [SerializeField, Expandable] TouchLayoutHandler _touchLayoutHandler;
    public TouchLayoutHandler TouchLayoutHandler => _touchLayoutHandler;

    public GameSettings Settings => _gameSettings;
}
