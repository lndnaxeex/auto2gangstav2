﻿using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu]
public class GameSettings : ScriptableObject
{
    [SerializeField] bool _useOnScreenInput = true;
    [SerializeField] SceneSelector _initialScene;

    public string InitialSceneName
    {
        get { return _initialScene.SceneName;  }
    }

    public bool UseOnScreenInput
    {
        get { return _useOnScreenInput; }
    }

}