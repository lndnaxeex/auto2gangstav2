using System;
using UnityEngine; 
namespace Behaviors.Human
{
    [System.Serializable]
    public class StandardMovementInputControl 
    {
        [SerializeField] Inputs.Type _jumpButton;
        [SerializeField] Inputs.Type _crouchButton;
        [SerializeField] Inputs.Type _sprintButton;

        Transform m_Cam { 
            get { 
                if (m_Character.CameraStatus == null) return null;
                return m_Character.CameraStatus.Camera.transform; 
            } 
        }
        
        [NonSerialized]
        public StandardMovement m_Character;

        Vector3 m_Move;
        bool m_Jump;      
        Vector3 m_CamForward;                // the world-relative desired move direction, calculated from the camForward and user input.
        
        public void Update()
        {
            if (!m_Jump)
            {
                m_Jump = Inputs.Management.Current.Get(_jumpButton) > .5f;
            }
        // }


        // // Fixed update is called in sync with physics
        // private void FixedUpdate()
        // {
            // read inputs
            var h = Inputs.Management.Current.Get(Inputs.Type.LEFT_H);
            var v = Inputs.Management.Current.Get(Inputs.Type.LEFT_V);
            var crouch = Inputs.Management.Current.Get(_crouchButton) > .5; 

            // // calculate move direction to pass to character
            if (m_Cam != null)
            {
                // calculate camera relative direction to move:
                m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
                m_Move = v*m_CamForward + h*m_Cam.right;
            }
            else
            {
                // we use world-relative directions in the case of no main camera
                m_Move = v*Vector3.forward + h*Vector3.right;
            }
            //m_Move = v*Vector3.forward + h*Vector3.right;
// #if !MOBILE_INPUT
			// walk speed multiplier
	        if (Inputs.Management.Current.Get(_sprintButton) > .5) m_Move *= 0.5f;
// #endif

            // pass all parameters to the character control script
            m_Character.Move(m_Move, crouch, m_Jump);
            m_Jump = false;
        }
    }

}