using UnityEngine;

namespace Behaviors.Human
{
    public abstract class BehaviorProfile : ScriptableObject {}
    
    [CreateAssetMenu]
    public class HumanBehaviorProfile : BehaviorProfile
    {
        HumanStatus _humanStatus;
        [SerializeField] HumanBehaviorBase _defaultBehavior;
        [SerializeField] HumanBehaviorBase[] _behaviors;

        HumanBehaviorBase _current;

        public void Setup(HumanStatus humanStatus)
        {
            _humanStatus = humanStatus;

            _defaultBehavior.Setup(_humanStatus);
            SetCurrent(_defaultBehavior);

            for (var x = 0; x < _behaviors.Length; x++)
            {
                _behaviors[x].Setup(_humanStatus);
            }
        }
        
        public void Update()
        {
            // switch current
            for (var x = 0; x < _behaviors.Length; x++)
            {
                if (Inputs.Management.Current.Get(_behaviors[x].SwitchButton) > .1f)
                {
                    if (_current == _behaviors[x]) _current = _defaultBehavior;
                    else SetCurrent(_behaviors[x]);    
                }
            }
            _current.Update();
        }

        void SetCurrent(HumanBehaviorBase behvaior)
        {
            // animator controller + current var
            _current = behvaior;
            _humanStatus.Components.Animator.runtimeAnimatorController = _current.AnimatorController;
        }
    }
}