using UnityEngine;
namespace Behaviors
{
    public abstract class BehaviorBase : ScriptableObject
    {
    }


    public abstract class HumanBehaviorBase : BehaviorBase
    {
        protected Transform transform { get { return _humanStatus.Components.Transform; } }
        protected HumanStatus _humanStatus;
        [SerializeField] RuntimeAnimatorController _controller;
        public RuntimeAnimatorController AnimatorController { get {return _controller;}}
        [SerializeField] Inputs.Type _switchButton;
        public Inputs.Type SwitchButton {
            get { return _switchButton; }
        }
        public virtual void Setup(HumanStatus humanPlayerStatus)
        {
            _humanStatus = humanPlayerStatus;
        }

        public abstract void Update();
    }
}
