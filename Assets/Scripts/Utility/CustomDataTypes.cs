/// <summary>
/// Editor data types for easy scene/prefab selection
/// </summary>

[System.Serializable]
public class SceneSelector
{
    public string SceneName;
}

[System.Serializable]
public class PrefabSelector
{
    public string PrefabPath;
}
